package android.dayman.crystalball;


import java.util.Random;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
          "Your Wishes Will Come True!",
                "Your Wishes Will NEVER COME TRUE!",
                "Maybe some day..",
                "It's best to ask another question.."
        };
    }

    public static Predictions get() {
        if (predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }

    public  String getPrediction(){
        int rnd = new Random().nextInt(answers.length);
        return answers[rnd];
    }

}
